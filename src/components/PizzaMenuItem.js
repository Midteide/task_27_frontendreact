import React from "react";
import Col from "react-bootstrap/Col";

const PizzaMenuItem = props => {
	let pizza = props.pizza;
	let _URL = props.URL;

	const handleDelete = e => {
		let URL = `${_URL}/person/${pizza.id}`;
		fetch(URL, {
			method: "DELETE",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			} //,
			// body: JSON.stringify({
			//     firstParam: 'yourValue',
			//     secondParam: 'yourOtherValue',
			// })
		}).then(resp => {});
	};
	return (
		// Adding columnt for each character, with responsiveness for different screen sizes
		<Col xs={12} sm={6} md={4} className="" key={pizza.id}>
			<div className="card z-depth-3" id="cardd">
				<a href={`/pizza/${pizza.id}/${props.order}`}>
					<div className="card-image">
						<img src={pizza.img} className="z-depth-4 bilde" alt={pizza.pizzaname} />
					</div>
				</a>
				<a className="mt-5 green waves-effect waves-light btn" href={`/pizza/${pizza.id}/${props.order}`}>
					Choose
				</a>
				<a href={`/pizza/${pizza.id}`}>
					<div className="card-content">
						<p className="text-center">No. {pizza.id}:</p>
						<p className="text-center">
							<b>{pizza.pizzaname}</b>
						</p>
						<br />
						<p className="text-center">({pizza.description})</p>
					</div>
				</a>
			</div>
		</Col>
	);
};

export default PizzaMenuItem;
