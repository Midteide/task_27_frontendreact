import React, { useState, useEffect } from "react";
// import Col from "react-bootstrap/Col";
// import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Styles from "./OrderDetails.css";

function Receipt(props) {
	const [currentOrder, setCurrentOrder] = useState(null);
	let URL = "https://pizzadb-noroff.herokuapp.com";
	const [doUpdate, setDoUpdate] = useState(false);
	const [cancelled, setCancelled] = useState(false);

	useEffect(() => {
		console.log("Receipt order: ", props.match.params.orderid);
		if (doUpdate && !isNaN(props.match.params.orderid)) {
			fetch(URL + "/order/" + props.match.params.orderid)
				.then(resp => resp.json())
				.then(resp => {
					// setCurrentCustomer(resp.customer);
					console.log("RECEIPT MOTTOK: ", resp);
					setCurrentOrder(resp);
					setDoUpdate(false);
				})
				.catch(err => console.error(err));
		}
		// eslint-disable-next-line
	}, [doUpdate]);

	useEffect(() => {
		let _URL = `${URL}/submitorder/${props.match.params.orderid}`;
		console.log("SUBMITTING to: ", _URL);
		fetch(_URL, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			} //,
			// body: JSON.stringify({
			//     firstParam: 'yourValue',
			//     secondParam: 'yourOtherValue',
			// })
		})
			.then(resp => resp.json())
			.then(resp => {
				console.log("SUBMIT resp: ", resp);
				if (resp !== -1) {
					setDoUpdate(true);
				}
			});
		// eslint-disable-next-line
	}, []);

	const handleCancelOrder = id => {
		let _URL = `${URL}/order/${props.match.params.orderid}`;
		fetch(_URL, {
			method: "DELETE",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			}
		}).then(resp => {
			setCancelled(true);
		});
	};

	const renderOrderDetails = () => {
		if (currentOrder) {
			const { customer } = currentOrder;
			return (
				<>
					<p>Deliver to:</p>
					<p className="green-text">
						<i>
							<b>
								{customer.first_name} {customer.last_name} {customer.address}
							</b>
						</i>
					</p>
					<h5 className="mt-4 mb-4">You ordered:</h5>
					{console.log("pizzas:", currentOrder.pizzas)}
					{currentOrder.pizzas.length === 0 && "None items added yet."}
					{!(currentOrder.pizzas.length === 0) &&
						currentOrder.pizzas.map(pizza => (
							<p className="mb-3" key={pizza.id}>
								<b>{pizza.pizzaname}</b> <br /> ({pizza.description}) <br />
								{pizza.large && <span className="red-text">Large. </span>}
								{pizza.extracheese && <span className="red-text">Extra cheese. </span>}
								{pizza.extrameat && <span className="red-text">Extra meat. </span>}
								Price: ${pizza.price}
								<hr />
							</p>
						))}
					<h4>
						Total price: <b>${currentOrder.price}</b>
					</h4>
				</>
			);
		}
	};
	return (
		<Container>
			{currentOrder && (
				<div className="row">
					<div className="col col-xs-12 OrderBox text-center">
						<div className="card blue-grey darken-1 OrderBox">
							<div className="card-content white-text OrderBox">
								<h1>Thanks for your order, {currentOrder.customer.first_name}!</h1>
								<span className="card-title">Receipt for Order no.: #{currentOrder.order.id}</span>
								<p className="">Order created: {currentOrder.order.createdDate}</p>
								<p className="mb-5">Order submitted: {currentOrder.order.submittedDate}</p>
								{renderOrderDetails()}
							</div>
							<div className="card-action OrderBox">
								{!cancelled && (
									<h5>
										<a onClick={handleCancelOrder} href="#">
											Cancel order <br />
											<i className="medium material-icons">delete</i>
										</a>
									</h5>
								)}
								{cancelled && <h5 className="red-text">ORDER CANCELLED!</h5>}
							</div>
						</div>
					</div>
				</div>
			)}
		</Container>
	);
}

export default Receipt;
