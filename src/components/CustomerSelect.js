import React, { useState, useEffect } from "react";
import Styles from "./Chart.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

const CustomerSelect = props => {
	let _URL = props.URL;
	const [customerList, setCustomerList] = useState();
	const [doFetch, setDoFetch] = useState(true);
	const [isUpdating, setIsUpdating] = useState(true);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [address, setAddress] = useState("");
	const [updateSuccessful, setUpdateSuccessful] = useState(null);

	useEffect(() => {
		fetch(props.URL + "/customer")
			.then(resp => resp.json())
			.then(resp => {
				setCustomerList(resp);
				console.log("CustomerSelect MOTTOK: ", resp);
				setDoFetch(false);
				setIsUpdating(false);
			})
			.catch(err => console.error(err));
		// eslint-disable-next-line
	}, [doFetch]);

	const handleSubmit = e => {
		e.preventDefault();
		//localhost:8080/customer/?firstname=Craig&lastname=Marais&address=Noroffvegen 44
		let URL = `${_URL}/customer/?firstname=${firstName}&lastname=${lastName}&address=${address}`;
		console.log("Will POST to: ", URL);

		fetch(URL, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			} //,
			// body: JSON.stringify({
			//     firstParam: 'yourValue',
			//     secondParam: 'yourOtherValue',
			// })
		})
			.then(resp => resp.json())
			.then(resp => {
				console.log("PUT RESP: ", resp);
				if (resp !== -1) setUpdateSuccessful(true);
				else setUpdateSuccessful(false);
				setDoFetch(true);
			})
			.catch(err => console.error(err));
	};

	const handleChange = customer => {
		console.log(customer);
		let URL = `${props.URL}/order/?customerId=${customer.id}`;

		fetch(URL, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			} //,
			// body: JSON.stringify({
			//     firstParam: 'yourValue',
			//     secondParam: 'yourOtherValue',
			// })
		})
			.then(resp => resp.json())
			.then(resp => {
				if (resp !== -1) {
					console.log("Setting customer: ", customer);
					props.setCustomer(customer);
					console.log("Setting order id: ", resp);
					props.setOrder(resp);
				}
			});
	};

	return (
		// Adding columnt for each character, with responsiveness for different screen sizes
		<Container className="mt-5">
			<Row>
				<Col>
					{doFetch && (
						<div className="text-center">
							<h3>Please wait while fething data...</h3>
							<div className="preloader-wrapper big active">
								<div className="spinner-layer spinner-blue-only">
									<div className="circle-clipper left">
										<div className="circle"></div>
									</div>
									<div className="gap-patch">
										<div className="circle"></div>
									</div>
									<div className="circle-clipper right">
										<div className="circle"></div>
									</div>
								</div>
							</div>
						</div>
					)}
				</Col>
			</Row>
			<Row>
				<Col>
					<div className="dropdown text-center">
						<button
							className="btn btn-secondary dropdown-toggle text-center"
							type="button"
							id="dropdownMenuButton"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="false"
							//onChange={handleChange}
						>
							Select your user here
						</button>
						<div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
							{customerList &&
								customerList.map(customer => {
									return (
										<a onClick={() => handleChange(customer)} key={customer.id} className="dropdown-item" href="#">
											{customer.last_name}, {customer.first_name} (ID: {customer.id})
										</a>
									);
								})}
						</div>
					</div>
				</Col>
			</Row>
			<Row>
				<h5> Or fill out below to register as a new customer first:</h5>
				<form id="personform" className="" onSubmit={handleSubmit}>
					<div className="row">
						<div className="first_name input-field col s6">
							<i className="material-icons prefix">account_circle</i>
							<input
								onChange={e => {
									setFirstName(e.target.value);
								}}
								value={firstName}
								id="first_name"
								type="text"
								className="validate"
								required
							/>
							<label htmlFor="first_name">First name</label>
						</div>
						<div className="last_name input-field col s6">
							<i className="material-icons prefix">account_circle</i>
							<input
								onChange={e => {
									setLastName(e.target.value);
								}}
								value={lastName}
								id="last_name"
								type="text"
								className="validate"
								required
							/>
							<label htmlFor="last_name">Last name</label>
						</div>
						<div className="address input-field col s6">
							<i className="material-icons prefix">account_circle</i>
							<input
								onChange={e => {
									setAddress(e.target.value);
								}}
								value={address}
								id="address"
								type="text"
								className="validate"
								required
							/>
							<label htmlFor="address">Address</label>
						</div>
					</div>
					<div className="row">
						<div className="col-12 center-align">
							<button className="btn waves-effect waves-light" type="submit" name="action">
								Add customer
								<i className="material-icons right">person_add</i>
							</button>
							{updateSuccessful === true && (
								<>
									{" "}
									<p className="green-text">Update successful!</p>
								</>
							)}
							{updateSuccessful === false && (
								<>
									{" "}
									<p className="red-text">Update failed!</p>
								</>
							)}
						</div>
					</div>
				</form>
			</Row>
		</Container>
	);
};

export default CustomerSelect;
