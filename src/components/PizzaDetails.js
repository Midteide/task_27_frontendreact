/* ##########################################################################################

This component prints the details of the pizzas pushed in the main component (pizzas).
The pizza to be printed is determined by the address field (/pizza/<id>)

########################################################################################## */

import React, { useEffect, useState } from "react";
import Styles from "./pizzaDetails.module.css";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";

function PizzaDetails(props) {
	const [pizza, setPizza] = useState({ id: 0, pizzaname: "", description: "", lg: "", ec: "", em: 0, orderID: 0 });
	const [isLarge, setIsLarge] = useState(false);
	const [extraCheese, setExtraCheese] = useState(false);
	const [extraMeat, setExtraMeat] = useState(false);
	const [doUpdate, setDoUpdate] = useState(true);
	const [updateSuccessful, setUpdateSuccessful] = useState(null);
	let URL = "https://pizzadb-noroff.herokuapp.com";

	let _URL = URL;

	useEffect(() => {
		let apiUrl = _URL + "/menu/" + props.match.params.pizzaid;
		console.log("PizzaDetails: Fetching from: ", apiUrl);
		if (doUpdate) {
			fetch(apiUrl)
				.then(resp => resp.json())
				.then(resp => {
					console.log("pizza: ", resp);
					setPizza(resp);
					setDoUpdate(false);
				})
				.catch(err => console.error(err));
		}
		// eslint-disable-next-line
	}, [doUpdate]);

	const handleSubmit = e => {
		e.preventDefault();
		//localhost:8080/pizza/?oId=1&pId=1&ec=true&em=true&lg=true
		let URL = `${_URL}/pizza/?oId=${props.match.params.orderid}&pId=${props.match.params.pizzaid - 1}&ec=${extraCheese}&em=${extraMeat}&lg=${isLarge}`;
		console.log("Will POST to: ", URL);

		fetch(URL, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			} //,
			// body: JSON.stringify({
			//     firstParam: 'yourValue',
			//     secondParam: 'yourOtherValue',
			// })
		})
			.then(resp => resp.json())
			.then(resp => {
				console.log("PUT RESP: ", resp);
				setDoUpdate(true);
				if (resp !== -1) setUpdateSuccessful(true);
				else setUpdateSuccessful(false);
			})
			.catch(err => console.error(err));
	};

	const handleDelete = e => {
		let URL = `${_URL}/pizza/${pizza.id}`;
		fetch(URL, {
			method: "DELETE",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			}
		}).then(resp => {
			window.location.href = "/";
		});
	};

	const renderpizza = () => {
		if (pizza) {
			return (
				// <Container>
				<Row>
					<Col className="text-center">
						<img className="z-depth-4" id={Styles.pizzaItemImg} src={pizza.img} alt={pizza.name} />
						<br />
						<a href={`/${props.match.params.orderid}`} className="mt-5 waves-effect waves-light btn">
							<i className="material-icons left">home</i>Back to menu
						</a>
						<br />
					</Col>
					<Col className="text-left">
						<form id="pizzaform" className="" onSubmit={handleSubmit}>
							<div className="row ml-5">
								<div className="switch">
									<label>
										Normal
										<input onChange={e => setIsLarge(e.target.checked)} type="checkbox" />
										<span className="lever"></span>
										Large (+$4.00)
									</label>
								</div>
							</div>
							<div className="row ml-5">
								<div className="switch">
									<label>
										Normal
										<input onChange={e => setExtraCheese(e.target.checked)} type="checkbox" />
										<span className="lever"></span>
										Extra cheese (+$2.50)
									</label>
								</div>
							</div>
							<div className="row ml-5">
								<div className="switch">
									<label>
										Normal
										<input onChange={e => setExtraMeat(e.target.checked)} type="checkbox" />
										<span className="lever"></span>
										Extra meat (+$3.50)
									</label>
								</div>
							</div>

							<div className="row  mt-4">
								<div className="col-12 ">
									<button className="btn waves-effect waves-light" type="submit" name="action">
										Add pizza to chart
										<i className="material-icons right">local_pizza</i>
										{/* <i className="material-icons right">add</i> */}
									</button>
									<br />
									{updateSuccessful === true && (
										<>
											{" "}
											<p className="green-text">Successfully added to chart!</p>
										</>
									)}
									{updateSuccessful === false && (
										<>
											{" "}
											<p className="red-text">Something went wrong...!</p>
										</>
									)}
								</div>
							</div>
						</form>
						{/* </Row> */}
					</Col>
				</Row>
				// </Container>
			);
		}
	};

	return (
		<div className="pizzaDetails container">
			<Row>
				<Col>
					{pizza && (
						<>
							<h1 className="text-center">
								No. {pizza.id}: {pizza.pizzaname}
							</h1>
							<h4 className="text-center">{pizza.description}</h4>
						</>
					)}
				</Col>
			</Row>

			{renderpizza()}
			<Row></Row>
		</div>
	);
}

export default PizzaDetails;
