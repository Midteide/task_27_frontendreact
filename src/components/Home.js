import React, { useState, useEffect } from "react";
import PizzaMenuList from "./PizzaMenuList";
import CustomerSelect from "./CustomerSelect";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import OrderDetails from "./OrderDetails";

function Home(props) {
	const [currentCustomer, setCurrentCustomer] = useState(null);
	const [currentOrder, setCurrentOrder] = useState(null);
	let URL = "https://pizzadb-noroff.herokuapp.com";

	useEffect(() => {
		console.log("props.match.params.orderid: ", props.match.params.orderid);
		if (props.match.params.orderid !== "undefined") {
			fetch(URL + "/order/" + props.match.params.orderid)
				.then(resp => resp.json())
				.then(resp => {
					setCurrentCustomer(resp.customer);
					console.log("HOME MOTTOK: ", resp);
					setCurrentOrder(props.match.params.orderid);
				})
				.catch(err => console.error(err));
		}
		// eslint-disable-next-line
	}, [props.match.params.orderid]);
	return (
		<Container>
			<p className="text-center">Task 27 by Alexander Midteide</p>
			<Row>
				<Col className="text-center">{currentOrder && <OrderDetails order={currentOrder} />}</Col>
			</Row>
			<Row>
				<Col>
					{!currentCustomer && (
						<CustomerSelect setCustomer={customer => setCurrentCustomer(customer)} setOrder={order => setCurrentOrder(order)} URL={URL} />
					)}
					{currentOrder && <PizzaMenuList order={currentOrder} customer={currentCustomer} URL={URL} />}
				</Col>
			</Row>
		</Container>
	);
}

export default Home;
