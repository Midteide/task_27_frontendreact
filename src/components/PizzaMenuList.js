import React, { useState, useEffect } from "react";
import PizzaMenuItem from "./PizzaMenuItem";
import "./PizzaMenuItem.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

function PizzaMenuList(props) {
	const [menuItems, setMenuItems] = useState();
	const [doFetch, setDoFetch] = useState(true);
	const [customer, setCustomer] = useState({
		first_name: ""
	});
	const [isUpdating, setIsUpdating] = useState(true);

	useEffect(() => {
		fetch(props.URL + "/menu")
			.then(resp => resp.json())
			.then(resp => {
				setMenuItems(resp);
				console.log("MOTTOK: ", resp);
				setDoFetch(false);
				setIsUpdating(false);
			})
			.catch(err => console.error(err));
		// eslint-disable-next-line
	}, []);

	useEffect(() => {
		setCustomer(props.customer);
		console.log("Setting customer to: ", props.customer);
		// eslint-disable-next-line
	}, [props.customer]);

	const renderMenu = e => {
		if (menuItems && menuItems.length)
			return menuItems.map(pizza => {
				console.log("PIZZAAA: ", props.order);
				return <PizzaMenuItem order={props.order} pizza={pizza} key={pizza.id} />;
			});
		else if (isUpdating)
			return (
				<div className="preloader-wrapper big active">
					<div className="spinner-layer spinner-blue-only">
						<div className="circle-clipper left">
							<div className="circle"></div>
						</div>
						<div className="gap-patch">
							<div className="circle"></div>
						</div>
						<div className="circle-clipper right">
							<div className="circle"></div>
						</div>
					</div>
				</div>
			);
		else
			return (
				<>
					<p>No results.</p>
				</>
			);
	};

	return (
		<div>
			<Container>
				<Row>
					<Col>
						<h4 className="text-center"> Hello {customer.first_name} - Choose your pizza! </h4>
					</Col>
				</Row>

				<Row>{renderMenu()}</Row>
			</Container>
		</div>
	);
}

export default PizzaMenuList;
