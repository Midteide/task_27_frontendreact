import React, { useState, useEffect, useRef } from "react";
import Styles from "./Chart.module.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

const Chart = props => {
	let pizza = props.pizza;
	const { person } = props;
	let _URL = props.URL;
	const [customerList, setCustomerList] = useState();
	const [doFetch, setDoFetch] = useState(true);
	const [isUpdating, setIsUpdating] = useState(true);
	const [searchTerm, setSearchTerm] = useState(null);

	useEffect(() => {
		fetch(props.URL + "/customer")
			.then(resp => resp.json())
			.then(resp => {
				const { results } = resp || [];
				//let temp = [...characters, ...results]
				setCustomerList(resp);
				console.log("CHART MOTTOK: ", resp);
				setDoFetch(false);
				setIsUpdating(false);
			})
			.catch(err => console.error(err));
		// eslint-disable-next-line
	}, []);

	const handleChange = e => {
		console.log(e.target);
	};

	return (
		// Adding columnt for each character, with responsiveness for different screen sizes
		<Container className="mt-5">
			<Row>
				<Col>
					<div class="dropdown">
						<button
							class="btn btn-secondary dropdown-toggle"
							type="button"
							id="dropdownMenuButton"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="false"
							onChange={handleChange}
						>
							Select your user here
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							{customerList &&
								customerList.map(customer => {
									return (
										<a key={customer.id} class="dropdown-item" href="#">
											{customer.last_name}, {customer.first_name} (ID: {customer.id})
										</a>
									);
								})}
						</div>
					</div>
				</Col>
			</Row>
		</Container>
	);
};

export default Chart;
