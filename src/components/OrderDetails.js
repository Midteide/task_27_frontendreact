import React, { useState, useEffect } from "react";
// import Col from "react-bootstrap/Col";
// import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Styles from "./OrderDetails.css";

function OrderDetails(props) {
	const [currentOrder, setCurrentOrder] = useState({
		customer: { first_name: "", last_name: "", address: "" },
		order: {
			id: 0
		},
		pizzas: []
	});
	let URL = "https://pizzadb-noroff.herokuapp.com";
	const [doUpdate, setDoUpdate] = useState(true);

	useEffect(() => {
		console.log("OrderDetails order: ", props.order);
		if (props.order !== isNaN(props.order) && doUpdate) {
			fetch(URL + "/order/" + props.order)
				.then(resp => resp.json())
				.then(resp => {
					// setCurrentCustomer(resp.customer);
					console.log("ORDERDETAILS MOTTOK: ", resp);
					setCurrentOrder(resp);
					setDoUpdate(false);
				})
				.catch(err => console.error(err));
		}
		// eslint-disable-next-line
	}, [props.order, doUpdate]);

	const handleDeletePizza = id => {
		let _URL = `${URL}/pizza/${id}`;
		fetch(_URL, {
			method: "DELETE",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json"
			}
		}).then(resp => {
			setDoUpdate(true);
		});
	};

	const renderOrderDetails = () => {
		if (currentOrder) {
			const { customer } = currentOrder;
			return (
				<>
					<p>
						Deliver to: <br />
						<b>
							{customer.first_name} {customer.last_name} {customer.address}
						</b>
					</p>
					<h5 className="mt-4">Current chart:</h5>
					{console.log("pizzas:", currentOrder.pizzas)}
					{currentOrder.pizzas.length === 0 && "No items added yet."}
					{!(currentOrder.pizzas.length === 0) &&
						currentOrder.pizzas.map(pizza => (
							<p className="mt-5" key={pizza.id}>
								{" "}
								<b>
									<u>{pizza.pizzaname}</u>
								</b>{" "}
								<br /> ({pizza.description}) <br />
								{pizza.large && <span className="red-text">Large. </span>}
								{pizza.extracheese && <span className="red-text">Extra cheese. </span>}
								{pizza.extrameat && <span className="red-text">Extra meat. </span>}
								Price: ${pizza.price}
								<br />
								<a className=" " href="javascript:void(0);" onClick={() => handleDeletePizza(pizza.id)}>
									<i id={Styles.iconstyle} className="material-icons ikon">
										cancel
									</i>
								</a>
							</p>
						))}

					<h4>
						Total price: <b>${currentOrder.price}</b>
					</h4>
				</>
			);
		}
	};
	return (
		<Container>
			{currentOrder && (
				<div className="row">
					<div className="col s12 m6 OrderBox">
						<div className="card blue-grey darken-1 OrderBox">
							<div className="card-content white-text OrderBox">
								<span className="card-title">Order details (Order no.: #{currentOrder.order.id})</span>
								{renderOrderDetails()}
							</div>
							<div className="card-action OrderBox">
								<h5>
									<a href={`/receipt/${currentOrder.order.id}`}>
										Place order <br />
										<i className="medium material-icons">add_shopping_cart</i>
									</a>
								</h5>
							</div>
						</div>
					</div>
				</div>
			)}
		</Container>
	);
}

export default OrderDetails;
