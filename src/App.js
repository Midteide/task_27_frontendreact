import React from "react";
import "./App.css";
import PizzaDetails from "./components/PizzaDetails";
import Receipt from "./components/Receipt";
import Home from "./components/Home";
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.min.js";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
	return (
		<Router>
			<div className="App">
				<Switch>
					<Route path="/pizza/:pizzaid/:orderid" component={PizzaDetails} />
					{/* <Route path="/" component={Home} /> */}
					<Route path="/receipt/:orderid" component={Receipt} />
					<Route path="/:orderid?" component={Home} />
					{/* <Home URL={URL} />
					</Route> */}
				</Switch>
			</div>
		</Router>
	);
}

export default App;
